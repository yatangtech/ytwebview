//
//  YTAppDelegate.h
//  YTWebView
//
//  Created by Jeavil_Tang on 05/15/2017.
//  Copyright (c) 2017 Jeavil_Tang. All rights reserved.
//

@import UIKit;

@interface YTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
