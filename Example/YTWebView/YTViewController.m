//
//  YTViewController.m
//  YTWebView
//
//  Created by Jeavil_Tang on 05/15/2017.
//  Copyright (c) 2017 Jeavil_Tang. All rights reserved.
//

#import "YTViewController.h"
#import <YTWebView/YTWebView.h>

@interface YTViewController ()<YTWebViewDelegate>

@end

@implementation YTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    YTWebView *webview = [[YTWebView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    [webview loadURLString:@"https://www.baidu.com"];
    webview.delegate = self;
    [self.view addSubview:webview];
}

- (void)ytWebViewDidStartLoad:(YTWebView *)webView {
    
}

- (void)ytWebView:(YTWebView *)webView didFinishLoadingURL:(NSURL *)URL {
    
}

- (void)ytWebView:(YTWebView *)webview shouldStartLoadWithURL:(NSURL *)URL {
    
}

- (void)ytWebView:(YTWebView *)webView didFailLoadWithURL:(NSURL *)url error:(NSError *)error {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
