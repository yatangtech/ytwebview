//
//  main.m
//  YTWebView
//
//  Created by Jeavil_Tang on 05/15/2017.
//  Copyright (c) 2017 Jeavil_Tang. All rights reserved.
//

@import UIKit;
#import "YTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YTAppDelegate class]));
    }
}
