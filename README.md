# YTWebView

[![CI Status](http://img.shields.io/travis/Jeavil_Tang/YTWebView.svg?style=flat)](https://travis-ci.org/Jeavil_Tang/YTWebView)
[![Version](https://img.shields.io/cocoapods/v/YTWebView.svg?style=flat)](http://cocoapods.org/pods/YTWebView)
[![License](https://img.shields.io/cocoapods/l/YTWebView.svg?style=flat)](http://cocoapods.org/pods/YTWebView)
[![Platform](https://img.shields.io/cocoapods/p/YTWebView.svg?style=flat)](http://cocoapods.org/pods/YTWebView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTWebView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YTWebView"
```

## Author

Jeavil_Tang, tangjiahui@yatang.cn

## License

YTWebView is available under the MIT license. See the LICENSE file for more info.
