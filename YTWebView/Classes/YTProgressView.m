//
//  YTProgressView.m
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/15.
//
//

#import "YTProgressView.h"
#import "YTWebView.h"

@interface YTProgressView ()
@property (nonatomic, strong) NSTimer *fakeProgressTimer;
@end

@implementation YTProgressView


- (void)setUp {
    [self setTrackTintColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    UIColor *tintColor = [UIColor colorWithRed:22.f / 255.f green:126.f / 255.f blue:251.f / 255.f alpha:1.0];
    [self setTintColor:tintColor];
}


- (void)fakeProgressViewStartLoading {
    [self setProgress:0.0f animated:NO];
    [self setAlpha:1.0f];
    
    if(!self.fakeProgressTimer) {
        self.fakeProgressTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f/60.0f target:self selector:@selector(fakeProgressTimerDidFire:) userInfo:nil repeats:YES];
    }
}

- (void)fakeProgressBarStopLoading {
    if(self.fakeProgressTimer) {
        [self.fakeProgressTimer invalidate];
    }
    
    if(self) {
        [self setProgress:1.0f animated:YES];
        [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self setProgress:0.0f animated:NO];
        }];
    }
}

- (void)fakeProgressTimerDidFire:(id)sender {
    CGFloat increment = 0.005/(self.progress + 0.2);
    if([_webView.superview.superview isKindOfClass:[UIWebView class]]) {
        CGFloat progress = (self.progress < 0.75f) ? self.progress + increment : self.progress + 0.0005;
        if(self.progress < 0.95) {
            [self setProgress:progress animated:YES];
        }
    }
}


@end
