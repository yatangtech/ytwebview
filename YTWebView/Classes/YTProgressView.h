//
//  YTProgressView.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/15.
//
//

#import <UIKit/UIKit.h>
@class YTWebView;

@interface YTProgressView : UIProgressView

@property (nonatomic, strong) YTWebView *webView;

/**
 * 开始加载
 */
- (void)fakeProgressViewStartLoading;
/**
 * 停止加载
 */
- (void)fakeProgressBarStopLoading;
/**
 * 开始计时
 */
- (void)fakeProgressTimerDidFire:(id)sender;
@end
