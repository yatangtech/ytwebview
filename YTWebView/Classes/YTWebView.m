//
//  YTWebView.m
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/15.
//
//

#import "YTWebView.h"
#import "YTProgressView.h"

#define  iOS_8 [[[UIDevice currentDevice] systemVersion] floatValue]>=8.0
static void *YTWebBrowserContext = &YTWebBrowserContext;

@interface YTWebView ()<UIAlertViewDelegate>

@property (nonatomic, strong) NSURL *URLToLaunchWithPermission;
@property (nonatomic, strong) UIAlertView *externalAppPermissionAlertView;
@property (nonatomic, assign) BOOL uiWebViewIsLoading;
@property (nonatomic, strong) YTProgressView *progressView;

@end

@implementation YTWebView

- (void)dealloc {
    [self.uiWebView setDelegate:nil];
    [self.wkWebView setNavigationDelegate:nil];
    [self.wkWebView setUIDelegate:nil];
    [self.wkWebView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        if (iOS_8) {
            self.wkWebView = [[WKWebView alloc] initWithFrame:frame];
        } else {
            self.uiWebView = [[UIWebView alloc] initWithFrame:frame];
        }
        
        if (self.wkWebView) {
            [self.wkWebView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            self.wkWebView.navigationDelegate = self;
            self.wkWebView.multipleTouchEnabled = YES;
            self.wkWebView.autoresizesSubviews = YES;
            [self addSubview:self.wkWebView];
            [self.wkWebView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:YTWebBrowserContext];
            
        } else {
            [self.uiWebView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            self.uiWebView.multipleTouchEnabled = YES;
            self.uiWebView.autoresizesSubviews = YES;
            self.uiWebView.scalesPageToFit = YES;
            self.uiWebView.delegate = self;
            [self addSubview:self.uiWebView];
        }
        [self setUpProgressView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame type:(YTWebViewType)type{
    self = [super initWithFrame:frame];
    if (self) {
        if (type == YTWKWebView) {
            self.wkWebView = [[WKWebView alloc] initWithFrame:frame];
        } else {
            self.uiWebView = [[UIWebView alloc] initWithFrame:frame];
        }
        
        if (self.wkWebView) {
            [self.wkWebView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            self.wkWebView.navigationDelegate = self;
            self.wkWebView.multipleTouchEnabled = YES;
            self.wkWebView.autoresizesSubviews = YES;
            [self addSubview:self.wkWebView];
            [self.wkWebView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:YTWebBrowserContext];
            
        } else {
            [self.uiWebView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            self.uiWebView.multipleTouchEnabled = YES;
            self.uiWebView.autoresizesSubviews = YES;
            self.uiWebView.scalesPageToFit = YES;
            self.uiWebView.delegate = self;
            [self addSubview:self.uiWebView];
        }
        [self setUpProgressView];
        
    }
    return self;
}

- (void)setUpProgressView {
    self.progressView = [[YTProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    self.progressView.tintColor = [UIColor colorWithRed:22.f / 255.f green:126.f / 255.f blue:251.f / 255.f alpha:1.0]; // iOS7 Safari bar color
    self.progressView.frame = CGRectMake(0, 0, self.frame.size.width, self.progressView.frame.size.height);
    self.progressView.trackTintColor = [UIColor colorWithWhite:1.0f alpha:0.0f];
    [self addSubview:self.progressView];
}

#pragma mark - Public Method

- (void)loadRequest:(NSURLRequest *)request {
    if (self.wkWebView) {
        [self.wkWebView loadRequest:request];
    }else {
        [self.uiWebView loadRequest:request];
    }
}

- (void)loadURL:(NSURL *)url {
    [self loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)loadURLString:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    [self loadURL:url];
}

- (void)loadHTMLString:(NSString *)htmlString {
    if (self.wkWebView) {
        [self.wkWebView loadHTMLString:htmlString baseURL:nil];
    } else {
        [self.uiWebView loadHTMLString:htmlString baseURL:nil];
    }
}


#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    if (webView == self.uiWebView) {
        [self.delegate ytWebViewDidStartLoad:self];
    }
}

// 发送请求之前，返回NO则不处理这个请求
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (webView == self.uiWebView) {
        if (![self externalAppRequiredToOpenURL:request.URL]) {
            self.uiWebViewIsLoading = YES;
            [self.progressView fakeProgressViewStartLoading];
            [self.delegate ytWebView:self shouldStartLoadWithURL:request.URL];
            return YES;
        } else {
            [self launchExternalAppWithURL:request.URL];
            return NO;
        }
    }
    return NO;
}

// 加载完成
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (webView == self.uiWebView) {
        if(!self.uiWebView.isLoading) {
            self.uiWebViewIsLoading = NO;
            [self.progressView fakeProgressBarStopLoading];
        }
        [self.delegate ytWebView:self didFinishLoadingURL:self.uiWebView.request.URL];
    }
}

// 加载失败
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if (webView == self.uiWebView) {
        if(!self.uiWebView.isLoading) {
            self.uiWebViewIsLoading = NO;
            [self.progressView fakeProgressBarStopLoading];
        }
        
        [self.delegate ytWebView:self didFailLoadWithURL:self.uiWebView.request.URL error:error];
    }
}

#pragma mark - WKNativeionDelegate

// 页面开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    if (webView == self.wkWebView) {
        
        [self.delegate ytWebViewDidStartLoad:self];
    }
}

// 加载完成
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (webView == self.wkWebView) {
        [self.delegate ytWebView:self didFinishLoadingURL:self.wkWebView.URL];
    }
}

// 当一个正在提交的页面在跳转过程中出现错误时调用这个方法
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    if (webView == self.wkWebView) {
        [self.delegate ytWebView:self didFailLoadWithURL:self.wkWebView.URL error:error];
    }
}

//  启动时加载数据发生错误就会调用这个方法
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(nonnull NSError *)error {
    if (webView == self.wkWebView) {
        [self.delegate ytWebView:self didFailLoadWithURL:self.wkWebView.URL error:error];
    }
}

// 发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if (webView == self.wkWebView) {
        NSURL *url = self.wkWebView.URL;
        if (![self externalAppRequiredToOpenURL:url]) {
            if (!navigationAction.targetFrame) {
                [self loadURL:url];
                decisionHandler(WKNavigationActionPolicyCancel);
                return;
            }
            [self callback_webViewShouldStartLoadWithRequest:navigationAction.request navigationType:navigationAction.navigationType];
        } else if ([[UIApplication sharedApplication] canOpenURL:navigationAction.request.URL]){
            [self launchExternalAppWithURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            decisionHandler(WKNavigationActionPolicyAllow);
            return;
        }
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}


#pragma mark - WKUIDelegate
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    if (navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

#pragma mark - Estimated Progress KVO (WKWebView)

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == self.wkWebView) {
        [self.progressView setAlpha:1.0f];
        BOOL animated = self.wkWebView.estimatedProgress > self.progressView.progress;
        [self.progressView setProgress:self.wkWebView.estimatedProgress animated:animated];
        
        // Once complete, fade out UIProgressView
        if(self.wkWebView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - External App Support
- (BOOL)externalAppRequiredToOpenURL:(NSURL *)URL {
    
    //若需要限制只允许某些前缀的scheme通过请求，则取消下述注释，并在数组内添加自己需要放行的前缀
//    if (self.validSchemes) {
//        return ![self.validSchemes containsObject:URL.relativePath];
//    }else {
    return !URL;
//    }
    
}

- (void)launchExternalAppWithURL:(NSURL *)URL {
    self.URLToLaunchWithPermission = URL;
    if (![self.externalAppPermissionAlertView isVisible]) {
        [self.externalAppPermissionAlertView show];
    }
    
}

-(BOOL)callback_webViewShouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(NSInteger)navigationType
{
    //back delegate
    [self.delegate ytWebView:self shouldStartLoadWithURL:request.URL];
    return YES;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView == self.externalAppPermissionAlertView) {
        if(buttonIndex != alertView.cancelButtonIndex) {
            [[UIApplication sharedApplication] openURL:self.URLToLaunchWithPermission];
        }
        self.URLToLaunchWithPermission = nil;
    }
}

@end

