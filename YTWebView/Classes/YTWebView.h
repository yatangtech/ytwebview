//
//  YTWebView.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/15.
//
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@class YTWebView;


typedef NS_ENUM(NSInteger, YTWebViewType)
{
    YTUIWebView,
    YTWKWebView
};


@protocol YTWebViewDelegate <NSObject>
@optional
/**
 * 准备加载页面
 * 参数：url
 */
- (void)ytWebView:(YTWebView *)webview shouldStartLoadWithURL:(NSURL *)URL;

/**
 * 开始加载页面
 * 参数：webView
 */
- (void)ytWebViewDidStartLoad:(YTWebView *)webView;

/**
 * 加载完成
 * 参数1:webview
 * 参数2: URL
 */
- (void)ytWebView:(YTWebView *)webView didFinishLoadingURL:(NSURL *)URL;

/**
 * 加载失败
 * 参数1: webview
 * 参数2: error
 */
- (void)ytWebView:(YTWebView *)webView didFailLoadWithURL:(NSURL *)url error:(NSError *)error;

@end

@interface YTWebView : UIView<UIWebViewDelegate,WKUIDelegate,WKNavigationDelegate>

@property (nonatomic, assign) id<YTWebViewDelegate>delegate;

@property (nonatomic, strong) UIWebView *uiWebView;
@property (nonatomic, strong) WKWebView *wkWebView;
@property (nonatomic, assign) CGRect navigationBarBounds;
@property (nonatomic, strong) NSSet *validSchemes;

#pragma mark - Initial Method
- (instancetype)initWithFrame:(CGRect)frame;
- (instancetype)initWithFrame:(CGRect)frame type:(YTWebViewType)type;


#pragma mark - Public Method

- (void)loadRequest:(NSURLRequest *)request;
- (void)loadURL:(NSURL *)url;
- (void)loadURLString:(NSString *)urlString;
- (void)loadHTMLString:(NSString *)htmlString;

@end
NS_ASSUME_NONNULL_END
